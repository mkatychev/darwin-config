"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" " Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let s:path = fnamemodify(resolve(expand('<sfile>:p')), ':h')
syntax off

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Prelude configs
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" auto resize split buffers in window resize
au! VimResized * wincmd =
" set t_8b=[48;2;%lu;%lu;%lum
" set t_8f=[38;2;%lu;%lu;%lum
" set t_8b=[48;2;%lu;%lu;%lum
" set t_8f=[38;2;%lu;%lu;%lum
" set noswapfile
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" LanguageClient
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
lua require('init')
" au! BufWritePre *.go,*.rs,*.svelte,*.js,*.ncl :lua vim.lsp.buf.format()
" au! BufWritePre *.scad :Scad
" " yaml inline langserver settings

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Filetype Aliases
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
au! BufNewFile,BufRead *.json,*.geojson set filetype=json
au! BufNewFile,BufRead *.shex set filetype=turtle
au! BufNewFile,BufRead *.fbs :syntax on
au! BufNewFile,BufRead .env* set filetype=sh
au! BufNewFile,BufRead .ignore set filetype=gitignore
au! BufNewFile,BufRead *.MD set filetype=markdown
au! BufNewFile,BufRead *.xml,*.plist set filetype=xml
au BufNewFile,BufRead *.s,*.S set filetype=arm | set tabstop=8
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Misc Global Vars
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:openbrowser_github_select_current_line = 1
" :checkhealth paths
let g:python3_host_prog = 'python3'
let g:python_host_prog = 'python2'
let g:ruby_host_prog = 'ruby'
" markdown config
" let g:vim_markdown_folding_disabled = 1
" let g:vim_markdown_conceal = 0
" let g:vim_markdown_fenced_languages = ['rust', 'go', 'python']
let g:vim_json_syntax_conceal = 0
let g:netrw_fastbrowse = 0

" autocmd FileType rs let b:surround_11="Option<\r>"
" autocmd FileType rs let b:surround_114="Result<\r>"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Don't lose visual selection when indenting
vmap > >gv
vmap < <gv
nmap Y y$
" system clipboard Put/Yank
map <C-P> "*P
nmap <C-p> "*p
map <C-Y> "*Y
map <C-y> "*y

" Yank entire buffer
noremap <silent><F4> gg"*yG<CR>
" Overwrite entire buffer with clipboard
noremap <silent><F5> :%d<CR>"*P<CR>
noremap <leader>q q
noremap q <Nop>

" <M-[>
noremap <D-i> dd<CR>
noremap <silent><M-w> :BufferClose<CR>

" fuzzy buffer search
noremap <silent><leader>/ <cmd>lua require('telescope.builtin').current_buffer_fuzzy_find({case_mod=ignore_case, disable_coordinates=true})<cr>
" map /  <Plug>(incsearch-forward)
" map ?  <Plug>(incsearch-backward)

" command mode emacs bindings
cnoremap <C-A> <C-B>
inoremap <C-A> <C-B>
" clear highlight
nnoremap <silent> <C-L> :nohl <bar> :lua vim.lsp.buf.clear_references()<CR>


" FZF commands
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.9 } }
" command! -bang -nargs=* Rg
"   \ call fzf#vim#grep(
"   \   'rg '.shellescape(<q-args>), 1,
"   \   fzf#vim#with_preview(), <bang>0)
"
" command! -bang -nargs=* Rguu
"   \ call fzf#vim#grep(
"   \   'rg -uu --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
"   \   fzf#vim#with_preview(), <bang>0)


command! -bang -nargs=? -complete=dir Fduu
  \ call s:fzf_command('fd -uu --type f --exclude .git --hidden --follow'.shellescape(<q-args>))

command! -bang -nargs=* GGrep
  \ call fzf#vim#grep(
  \   'git grep --line-number '.shellescape(<q-args>), 0,
  \   fzf#vim#with_preview({'dir': systemlist('git rev-parse --show-toplevel')[0]}), <bang>0)

command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(<q-args>, {'options': ['--layout=reverse', '--info=inline', '--preview', 'bat --style=numbers --color=always {} | head -500']}, <bang>0)

function! s:fzf_command(...)
   let args = copy(a:000)
   let cmd = copy(args[0])
   call remove(args, 0)
   let prev_default_command = $FZF_DEFAULT_COMMAND
   let $FZF_DEFAULT_COMMAND = cmd
   call fzf#vim#files(args, {'options': ['--layout=reverse', '--info=inline', '--preview', 'bat --style=numbers --color=always {} | head -500']})
   let $FZF_DEFAULT_COMMAND = prev_default_command
endfunction

" autocmd User TelescopeFindPre setlocal nonumber norelativenumber
nnoremap <C-F>f <cmd>Telescope fd<CR>
" nnoremap <C-F>f :Files<CR>
noremap <C-F>m <cmd>Telescope keymaps<CR>
noremap <C-F>b :Buffers <CR>
nnoremap <space>b <cmd>Telescope buffers<cr>
noremap <C-F>h :History:<CR>
noremap <C-F>c :Commits <CR>
noremap <C-F>/ <cmd>lua require('telescope.builtin').live_grep({grep_open_files=true, disable_coordinates=true})<cr>
noremap <C-F>r <cmd> :Rg <CR>
" noremap <C-F>g :GGrep <CR>

noremap <C-\> :TagbarToggle <CR>

xnoremap <silent><leader>c :'<,'>GetCommitLink<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Functions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

vnoremap <silent> <F8> d:execute 'normal i' . join(sort(split(getreg('"'))), ' ')<CR>

" Prevent wrapping from breaking up words
command! Doc :set wrap linebreak nolist

" Formatters
function! Format(formatter) range
  execute a:firstline . "," . a:lastline . "!" . a:formatter
endfunction

function! PosFormat(formatter) abort range
    " " Check whether a range was passed to the function
    if exists('a:firstline')
        execute 'keepjumps keeppatterns ' .
                    \ a:firstline . "," . a:lastline . "!" . a:formatter
    else
        execute 'keepjumps keeppatterns ' .
                    \ "!" . a:formatter
    endif
    " " Restore the state of the window (useful for not losing cursor position)
    let b:str_pos = printf("%s", b:pos)
    execute 'keepjumps ' . 'call winrestview(' . b:str_pos . ')'
endfunction

command! -range=% Sqlf let b:pos = winsaveview() |
            \ <line1>,<line2>call Format2("sql-formatter --config ~/.sqlf_config.json")
command! -range=% Jq <line1>,<line2>call Format("jq")
command! -range=% Typos <line1>,<line2>call Format("typos -w -")
command! -range=% Shf <line1>,<line2>call Format("shfmt -i 2 -sr")
command! -range=% Yq <line1>,<line2>call Format("yamkix -n -s")
command! -range=% Yqk <line1>,<line2>call Format("yamkix -s -n")

command! -range=% Lua :FormatCmd "stylua --indent-width 2 --indent-type Spaces  --collapse-simple-statement Always --call-parentheses NoSingleTable -"
command! -range=% Scad :FormatCmd "scadformat --log-level error"
command! -range=% -nargs=1 FormatCmd let b:pos = winsaveview() | <line1>,<line2>call PosFormat(<args>)




" The function switches all windows pointing to the current buffer (that you are closing)
" to the next buffer (or a new buffer if the current buffer is the last one).
"
" https://stackoverflow.com/questions/4298910/vim-close-buffer-but-not-split-windowanswer-29236158
function! CloseBuffer()
    let curBuf = bufnr('%')
    let curTab = tabpagenr()
    exe 'bnext'

    " If in last buffer, create empty buffer
    if curBuf == bufnr('%')
        exe 'enew'
    endif

    " Loop through tabs
    for i in range(tabpagenr('$'))
        " Go to tab (is there a way with inactive tabs?)
        exe 'tabnext ' . (i + 1)
        " Store active window nr to restore later
        let curWin = winnr()
        " Loop through windows pointing to buffer
        let winnr = bufwinnr(curBuf)
        while (winnr >= 0)
            " Go to window and switch to next buffer
            exe winnr . 'wincmd w | bnext'
            " Restore active window
            exe curWin . 'wincmd w'
            let winnr = bufwinnr(curBuf)
        endwhile
    endfor

    " Close buffer, restore active tab
    exe 'bd' . curBuf
    exe 'tabnext ' . curTab
endfunction



" augroup remember_folds
"   autocmd!
"   autocmd BufWinLeave *.* mkview
"   autocmd BufWinEnter *.* silent! loadview
" augroup END
"
function Gshow(branch)
     let file = expand('%')
     exe 'Gedit ' . a:branch . ':' . file
endfunction

command! -nargs=1 -complete=file Gshow call Gshow('<args>')



function! MoveFile(newspec)
     let old = expand('%')
     " could be improved:
     if (old == a:newspec)
         return 0
     endif
     exe 'sav' fnameescape(a:newspec)
     call delete(old)
endfunction

command! -nargs=1 -complete=file -bar MoveFile call MoveFile('<args>')

" https://stackoverflow.com/questions/63906439/how-to-disable-line-numbers-in-neovim-terminal
autocmd TermOpen * setlocal nonumber norelativenumber
