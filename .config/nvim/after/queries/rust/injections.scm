; extends

; Highlight SQL in `sqlx::query!()`, `sqlx::query_scalar!()`, and `sqlx::query_scalar_unchecked!()`
(macro_invocation
  macro: (scoped_identifier
    path: (identifier) @_sqlx (#eq? @_sqlx "sqlx")
    name: (identifier) @_query (#match? @_query "^query(_scalar|_scalar_unchecked)?$"))
  (token_tree
    ; Only the first argument is SQL
    .
    (raw_string_literal
      (string_content) @injection.content)
  )
  (#set! injection.language "sql"))

(call_expression
    function: (scoped_identifier
      path: (identifier) @_obj (#match? @_obj "^.*Safe(Pushable)?QueryBuilder$")
      name: (identifier) @_fn (#eq? @_fn "new"))
    arguments: (arguments (raw_string_literal (string_content) @injection.content))
  (#set! injection.language "sql"))
