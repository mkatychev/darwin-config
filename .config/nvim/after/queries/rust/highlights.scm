; extends

((identifier) @constant.builtin
  (#any-of? @constant.builtin "Some" "None" "Ok" "Err"))

[
  "const"
  "dyn"
  "move"
  "ref"
  "static"
] @keyword.modifier

(mutable_specifier) @keyword.storage.modifier.mut

(lifetime
  (identifier) @attribute.builtin
  (#any-of? @attribute.builtin "static" "_"))
