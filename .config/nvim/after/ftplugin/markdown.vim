let g:vim_markdown_conceal = 1
set conceallevel=3
setlocal expandtab
setlocal tabstop=2 expandtab
setlocal shiftwidth=2 expandtab
setlocal softtabstop=2

autocmd BufEnter * if &buftype == '' | set conceallevel=0 | endif

