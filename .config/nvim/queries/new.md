```scad
module hull()
```
────────────────────────────────────────────────────────────────────────────────────────────────
<a href="https://en.wikibooks.org/wiki/File:Openscad_hull_example_1a.png" class="image"><img src=https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Openscad_hull_example_1a.png/200px-Openscad_hull_example_1a.png width=160.0 height=119.2/></a>

<a href="https://en.wikibooks.org/wiki/File:Openscad_hull_example_1a.png" class="internal" title="Enlarge"></a>

Two cylinders

<a href="https://en.wikibooks.org/wiki/File:Openscad_hull_example_2a.png" class="image"><img src=https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Openscad_hull_example_2a.png/200px-Openscad_hull_example_2a.png width=160.0 height=119.2/></a>

<a href="https://en.wikibooks.org/wiki/File:Openscad_hull_example_2a.png" class="internal" title="Enlarge"></a>

Convex hull of two cylinders

Displays the <a href="https://www.cgal.org/Manual/latest/doc_html/cgal_manual/Convex_hull_2/Chapter_main.html"
class="external text" rel="nofollow">convex hull</a> of child nodes.

Usage example:

```scad
hull() {
  translate([ 15, 10, 0 ]) circle(10);
  circle(10);
}

```

The Hull of 2D objects uses their projections (shadows) on the xy plane,
and produces a result on the xy plane. Their Z-height is not used in the
operation.

A note on limitations: Running `hull() { a(); b(); }` is the same as
`hull() { hull() a(); hull() b(); }` so unless you accept/want
`hull() a();` and `hull() b();`, the result will not match expectations.

## Combining transformations

When combining transformations, it is a sequential process, but going
right-to-left. That is

```scad
rotate(...) translate(...) cube(5) ;

```

would first move the cube, and then move it in an arc (while also
rotating it by the same angle) at the radius given by the translation.

```scad
translate(...) rotate(...) cube(5) ;

```

would first rotate the cube and then move it to the offset defined by
the translate.

<a href="https://en.wikibooks.org/wiki/File:Openscad_combined_transform.png" class="image"><img src=https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Openscad_combined_transform.png/200px-Openscad_combined_transform.png width=160.0 height=172.0/></a>

<a href="https://en.wikibooks.org/wiki/File:Openscad_combined_transform.png" class="internal" title="Enlarge"></a>

Combine two transforms

```scad
color("red") translate([ 0, 10, 0 ]) rotate([ 45, 0, 0 ])
  cube(5);
color("green") rotate([ 45, 0, 0 ]) translate([ 0, 10, 0 ])
  cube(5);

```

Retrieved from "<a href="https://en.wikibooks.org/w/index.php?title=OpenSCAD_User_Manual/Transformations&amp;oldid=4082820"
dir="ltr">https://en.wikibooks.org/w/index.php?title=OpenSCAD_User_Manual/Transformations&amp;oldid=4082820</a>"

[Category](https://en.wikibooks.org/wiki/Special:Categories "Special:Categories"):

-   [Book:OpenSCAD User
    Manual](https://en.wikibooks.org/wiki/Category:Book:OpenSCAD_User_Manual "Category:Book:OpenSCAD User Manual")

