(attribute
    (attribute_name) @tag.attribute (#eq? @tag.attribute "href")
    (quoted_attribute_value
        (attribute_value) @conceal
) (#set! conceal "ok"))
