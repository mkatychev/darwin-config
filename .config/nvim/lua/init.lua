local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup {
  {
    "vhyrro/luarocks.nvim",
    priority = 1000,
    config = true,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      local configs = require("nvim-treesitter.configs")

      configs.setup {
        ensure_installed = { "lua", "vim", "vimdoc", "query", "rust", "markdown", "yaml", "sql", "wit" },
        sync_install = false,
        highlight = { enable = true },
        indent = { enable = true },
      }
    end,
  },
  {
    { "akinsho/toggleterm.nvim", version = "*", config = true },
  },
  { "tree-sitter/tree-sitter-rust" },
  {
    "tree-sitter-grammars/tree-sitter-test",
    -- compile on your own on Windows
    build = "make parser/test.so",
    ft = "test",
    init = function()
      -- toggle dynamic language injection
      vim.g.tstest_dynamic_injection = true
      -- toggle full-width rules for test separators
      vim.g.tstest_fullwidth_rules = false
      -- set the highlight group of the rules
      vim.g.tstest_rule_hlgroup = "FoldColumn"
    end,
  },
  { "tyru/open-browser-github.vim", dependencies = "tyru/open-browser.vim" },
  { "tyru/open-browser.vim" },
  { "knsh14/vim-github-link" },
  { "mattn/webapi-vim" },
  { "itchyny/vim-gitbranch" },
  { "dcharbon/vim-flatbuffers" },
  { "ARM9/arm-syntax-vim" },
  { "mtdl9/vim-log-highlighting" },
  { "tpope/vim-repeat" },
  { "tpope/vim-fugitive" },
  { "tpope/vim-abolish" },
  { "godlygeek/tabular" },
  { "sunaku/tmux-navigate" },
  { "majutsushi/tagbar", build = "brew install universal-ctags" },
  { "L3MON4D3/LuaSnip" }, -- Snippets plugin
  { "gfanto/fzf-lsp.nvim" }, -- fzf for lsp
  { "hrsh7th/cmp-buffer" },
  { "hrsh7th/cmp-cmdline" },
  { "hrsh7th/cmp-nvim-lsp" }, -- LSP source for nvim-cmp
  { "hrsh7th/cmp-path" },
  { "hrsh7th/nvim-cmp" }, -- Autocompletion plugin
  { "jvgrootveld/telescope-zoxide" },
  { "natecraddock/telescope-zf-native.nvim" },
  { "neovim/nvim-lspconfig" }, -- Collection of configurations for built-in LSP client
  { "nvim-lua/plenary.nvim" },
  { "nvim-lua/popup.nvim" },
  { "nvim-lualine/lualine.nvim" },
  -- { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
  { "nvim-telescope/telescope-ui-select.nvim" },
  { "nvim-telescope/telescope.nvim" },
  { "ngalaiko/tree-sitter-go-template" },
  { "nvim-treesitter/nvim-treesitter-textobjects" },
  { "nvim-treesitter/playground" },
  -- { dir = "/Users/mkatychev/Documents/tree-sitter/just" },
  -- {"vmchale/just-vim"},
  { "junegunn/fzf.vim" },
  { "junegunn/fzf" },
  { "saadparwaiz1/cmp_luasnip" }, -- Snippets source for nvim-cmp
  -- { "MunifTanjim/rust-tools.nvim" },
  {
    "mrcjkb/rustaceanvim",
    version = "^4", -- Recommended
    ft = { "rust" },
  },
  { "romgrk/barbar.nvim", dependencies = { "nvim-tree/nvim-web-devicons" } },
  { "navarasu/onedark.nvim" },
  { "akinsho/git-conflict.nvim", version = "*" },
  { "numToStr/Comment.nvim" },
  -- use { "kylechui/nvim-surround", tag = "*" }
  { "echasnovski/mini.surround" },
  { dir = "/Users/mkatychev/Documents/lua/rainbow-delimiters.nvim" },
  -- { dir = "/Users/mkatychev/Documents/treee-sitter/wit" },
  { "nickel-lang/vim-nickel" },
  {
    "mawkler/refjump.nvim",
    keys = { ']r', '[r' }, -- Uncomment to lazy load
    opts = {},
  },
  {
    "ibhagwan/fzf-lua",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      require("fzf-lua").setup { winopts = {
        height = 0.90,
        width = 0.90,
      } }
    end,
  },
  {
    "andymass/vim-matchup",
    init = function()
      -- may set any options here
      vim.g.matchup_matchparen_offscreen = { method = "popup" }
    end,
  },
  {
    "salkin-mada/openscad.nvim",
    dependencies = "L3MON4D3/LuaSnip",
    config = function()
      require("openscad")
      -- load snippets, note requires
      vim.g.openscad_load_snippets = true
    end,
  },
  -- {
  --   "nvim-neorg/neorg",
  --   dependencies = { "luarocks.nvim" },
  --   lazy = false, -- Disable lazy loading as some `lazy.nvim` distributions set `lazy = true` by default
  --   config = function()
  --     local cfg = require("_neorg")
  --     cfg.setup()
  --   end,
  -- },
}

require("toggleterm").setup {
  -- size can be a number or function which is passed the current terminal
  size = 20,
  open_mapping = [[<M-b>]], -- or { [[<c-\>]], [[<c-¥>]] } if you also use a Japanese keyboard.
  hide_numbers = true, -- hide the number column in toggleterm buffers
  autochdir = false, -- when neovim changes it current directory the terminal will change it's own when next it's opened
  shade_terminals = false, -- NOTE: this option takes priority over highlights specified so if you specify Normal highlights you should set this to false
  highlights = {
    -- highlights which map to a highlight group name and a table of it's values
    -- NOTE: this is only a subset of values, any group placed here will be set for the terminal window split
    Normal = {
      guibg = "#a626a4",
    },
    NormalFloat = {
      link = "Normal",
    },
  },
  start_in_insert = true,
  insert_mappings = true, -- whether or not the open mapping applies in insert mode
  terminal_mappings = true, -- whether or not the open mapping applies in the opened terminals
  persist_size = true,
  persist_mode = true, -- if set to true (default) the previous terminal mode will be remembered
  direction = "float",
  close_on_exit = true, -- close the terminal window when the process exits
  -- Change the default shell. Can be a string or a function returning a string
  shell = vim.o.shell,
  auto_scroll = true, -- automatically scroll to the bottom on terminal output
  -- This field is only relevant if direction is set to 'float'
  float_opts = {
    -- The border key is *almost* the same as 'nvim_open_win'
    -- see :h nvim_open_win for details on borders however
    -- the 'curved' border is a custom border type
    -- not natively supported but implemented in this plugin.
    border = "single",
    -- like `size`, width, height, row, and col can be a number or function which is passed the current terminal
    title_pos = "center",
  },
  winbar = {
    enabled = false,
    name_formatter = function(term) --  term: Terminal
      return term.name
    end,
  },
}

vim.o.shiftwidth = 2
vim.o.tabstop = 2
vim.o.termguicolors = true
vim.o.foldlevelstart = 99
vim.opt.viewoptions:remove { "options", "folds" }
vim.o.langmap =
  "АБСДЕФГЧИЙКЛМНОПЯРСТУВШХЫЗ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,абсдефгчийклмнопярстувшхыз;abcdefghijklmnopqrstuvwxyz"
vim.o.signcolumn = "yes"
vim.o.compatible = false

vim.g.rustaceanvim = {
  server = {
    default_settings = {
      -- rust-analyzer language server configuration
      ["rust-analyzer"] = {
        checkOnSave = {
          command = "check",
          extraArgs = { "--all", "--target-dir", "/tmp/rust-analyzer-check" },
        },
        diagnostics = { disabled = "inactive-code" },
      },
    },
  },
}

-- require('leap').add_default_mappings()
require("mini.surround").setup {
  -- Add custom surroundings to be used on top of builtin ones. For more
  -- information with examples, see `:h MiniSurround.config`.
  custom_surroundings = nil,

  -- Duration (in ms) of highlight when calling `MiniSurround.highlight()`
  highlight_duration = 500,

  -- Module mappings. Use `''` (empty string) to disable one.
  mappings = {
    add = "qa", -- Add surrounding in Normal and Visual modes
    delete = "qd", -- Delete surrounding
    find = "qf", -- Find surrounding (to the right)
    find_left = "qF", -- Find surrounding (to the left)
    highlight = "qh", -- Highlight surrounding
    replace = "qr", -- Replace surrounding
    update_n_lines = "qn", -- Update `n_lines`

    suffix_last = "l", -- Suffix to search with "prev" method
    suffix_next = "n", -- Suffix to search with "next" method
  },

  -- Number of lines within which surrounding is searched
  n_lines = 20,

  -- Whether to respect selection type:
  -- - Place surroundings on separate lines in linewise mode.
  -- - Place surroundings on each line in blockwise mode.
  respect_selection_type = false,

  -- How to search for surrounding (first inside current line, then inside
  -- neighborhood). One of 'cover', 'cover_or_next', 'cover_or_prev',
  -- 'cover_or_nearest', 'next', 'prev', 'nearest'. For more details,
  -- see `:h MiniSurround.config`.
  search_method = "cover",

  -- Whether to disable showing non-error feedback
  silent = false,
}
require("Comment").setup {
  toggler = {
    ---Line-comment toggle keymap
    line = "<M-/>",
    ---Block-comment toggle keymap
    block = "<M-?>",
  },
  opleader = {
    ---Line-comment keymap
    line = "<M-/>",
    ---Block-comment keymap
    block = "<M-?>",
  },
}
-- require("neorg").setup()
vim.opt.expandtab = true
vim.opt.hidden = true
vim.opt.mouse = "a"
vim.opt.laststatus = 1
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

vim.opt.relativenumber = true
vim.opt.number = true

local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd
local command = vim.api.nvim_create_user_command

-- command('RgNew', h

-- RelNumInActiveNormalWindow
autocmd("InsertLeave", {
  callback = function() vim.opt.relativenumber = true end,
})
autocmd("InsertEnter", {
  callback = function() vim.opt.relativenumber = false end,
})

autocmd("BufWritePost", {
  group = vim.api.nvim_create_augroup("PACKER", { clear = true }),
  pattern = "plugins.lua",
  command = "source <afile> | PackerCompile",
})
augroup("RelNumInActiveNormalWindow", { clear = true })
autocmd({ "VimEnter", "WinEnter", "BufWinEnter" }, {
  group = "RelNumInActiveNormalWindow",
  callback = function()
    if vim.bo.modifiable == true and vim.bo.readonly == false then vim.opt_local.relativenumber = true end
  end,
})

autocmd("WinLeave", {
  group = "RelNumInActiveNormalWindow",
  callback = function()
    if vim.bo.modifiable == true and vim.bo.readonly == false then vim.opt_local.relativenumber = false end
  end,
})
-- Remove whitespace on save
autocmd("BufWritePre", {
  pattern = "",
  command = ":%s/\\s\\+$//e",
})
-- LspFormatter
autocmd("BufWritePre", {
  pattern = { "*.go", "*.rs", "*.svelte", "*.js", "*.ncl" },
  callback = function() vim.lsp.buf.format() end,
})

-- Highlight on yank
augroup("YankHighlight", { clear = true })
autocmd("TextYankPost", {
  group = "YankHighlight",
  callback = function() vim.highlight.on_yank { higroup = "IncSearch", timeout = "200" } end,
})

-- co — choose ours
-- ct — choose theirs
-- cb — choose both
-- c0 — choose none
-- ]x — move to previous conflict
-- [x — move to next conflict
require("git-conflict").setup {
  default_mappings = true, -- disable buffer local mapping created by this plugin
  default_commands = true, -- disable commands created by this plugin
  disable_diagnostics = false, -- This will disable the diagnostics in a buffer whilst it is conflicted
  highlights = { -- They must have background color, otherwise the default color will be used
    incoming = "DiffText",
    current = "DiffAdd",
  },
}

-- Set barbar's options
require("barbar").setup {
  -- Enable/disable animations
  animation = false,

  -- Enable/disable auto-hiding the tab bar when there is a single buffer
  auto_hide = false,

  -- Enable/disable current/total tabpages indicator (top right corner)
  tabpages = true,

  -- Excludes buffers from the tabline
  -- exclude_ft = {'javascript'},
  -- exclude_name = {'package.json'},

  icons = {
    -- Configure the base icons on the bufferline.
    buffer_index = true,
    buffer_number = false,
    button = false,
    -- Enables / disables diagnostic symbols
    diagnostics = {
      [vim.diagnostic.severity.ERROR] = { enabled = true },
      --   [vim.diagnostic.severity.WARN] = { enabled = false },
      --   [vim.diagnostic.severity.INFO] = { enabled = false },
      --   [vim.diagnostic.severity.HINT] = { enabled = true },
    },
    filetype = {
      -- Sets the icon's highlight group.
      -- If false, will use nvim-web-devicons colors
      custom_colors = true,

      -- Requires `nvim-web-devicons` if `true`
      enabled = false,
    },
    separator = { left = "▎", right = "" },

    -- Configure the icons on the bufferline when modified or pinned.
    -- Supports all the base icon options.
    modified = false,
    pinned = { button = "車", filename = true, separator = { right = "" } },

    -- Configure the icons on the bufferline based on the visibility of a buffer.
    -- Supports all the base icon options, plus `modified` and `pinned`.
    alternate = { filetype = { enabled = false } },
    current = { buffer_index = true },
    inactive = { button = "×" },
    visible = { modified = { buffer_number = false } },
  },

  -- If true, new buffers will be inserted at the start/end of the list.
  -- Default is to insert after current buffer.
  insert_at_end = false,
  insert_at_start = false,

  -- Sets the maximum padding width with which to surround each tab
  maximum_padding = 0,

  -- Sets the maximum buffer name length.
  maximum_length = 30,

  -- If set, the letters for each buffer in buffer-pick mode will be
  -- assigned based on their name. Otherwise or in case all letters are
  -- already assigned, the behavior is to assign letters in order of
  -- usability (see order below)
  semantic_letters = true,

  -- New buffer letters are assigned in this order. This order is
  -- optimal for the qwerty keyboard layout but might need adjustement
  -- for other layouts.
  letters = "asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP",

  -- Sets the name of unnamed buffers. By default format is "[Buffer X]"
  -- where X is the buffer number. But only a static string is accepted here.
  no_name_title = nil,
}
require("_utils")

-- Move to previous/next
nmap("<C-S-Tab>", "<Cmd>BufferPrevious<CR>")
nmap("<C-Tab>", "<Cmd>BufferNext<CR>")
nmap("<A-1>", "<Cmd>BufferGoto 1<CR>")
nmap("<A-2>", "<Cmd>BufferGoto 2<CR>")
nmap("<A-3>", "<Cmd>BufferGoto 3<CR>")
nmap("<A-4>", "<Cmd>BufferGoto 4<CR>")
nmap("<A-5>", "<Cmd>BufferGoto 5<CR>")
nmap("<A-6>", "<Cmd>BufferGoto 6<CR>")
nmap("<A-7>", "<Cmd>BufferGoto 7<CR>")
nmap("<A-8>", "<Cmd>BufferGoto 8<CR>")
nmap("<A-9>", "<Cmd>BufferGoto 9<CR>")
nmap("<A-0>", "<Cmd>BufferLast<CR>")
nmap("<A-p>", "<Cmd>BufferPin<CR>")
nmap("<A-S-p>", "<Cmd>BufferPick<CR>")
nmap("<A-<>", "<Cmd>BufferMovePrevious<CR>")
nmap("<A->>", "<Cmd>BufferMoveNext<CR>")
nmap("<leader>1", "<Cmd>BufferOrderByBufferNumber<CR>")
nmap("<leader>q", "<Cmd>BufferOrderByDirectory<CR>")
nmap("<leader>a", "<Cmd>BufferOrderByLanguage<CR>")
nmap("<leader>z", "<Cmd>BufferOrderByWindowNumber<CR>")
nmap("<leader>cd", function() return require("telescope").extensions.zoxide.list {} end)

nmap("<leader>cd", function() return require("telescope").extensions.zoxide.list {} end)

require("telescope").setup {
  extensions = {
    ["ui-select"] = {
      require("telescope.themes").get_dropdown {
        -- even more opts
      },
    },
    ["zf-native"] = {
      -- options for sorting file-like items
      file = {
        -- override default telescope file sorter
        enable = true,
        -- highlight matching text in results
        highlight_results = true,
        -- enable zf filename match priority
        match_filename = true,
      },

      -- options for sorting all other items
      generic = {
        -- override default telescope generic item sorter
        enable = true,

        -- highlight matching text in results
        highlight_results = true,

        -- disable zf filename match priority
        match_filename = false,
      },
    },
  },
  defaults = {
    mappings = {
      i = {
        -- ["<C-v>"] = { ":set norenmuber nonumber<CR> <Esc>", type = "command" },
        ["<Esc>"] = require("telescope.actions").close,
        ["<C-a>"] = { "<Home>", type = "command" },
        ["<C-e>"] = { "<End>", type = "command" },
      },
    },
  },
  pickers = {
    find_files = {
      find_command = { "fd", "--type", "f", "--exclude", ".git", "--hidden", "--follow" },
    },
  },
}

-- | Token     | Match type                 | Description                          |
-- | --------- | -------------------------- | ------------------------------------ |
-- | `sbtrkt`  | fuzzy-match                | Items that match `sbtrkt`            |
-- | `'wild`   | exact-match (quoted)       | Items that include `wild`            |
-- | `^music`  | prefix-exact-match         | Items that start with `music`        |
-- | `.mp3$`   | suffix-exact-match         | Items that end with `.mp3`           |
-- | `!fire`   | inverse-exact-match        | Items that do not include `fire`     |
-- | `!^music` | inverse-prefix-exact-match | Items that do not start with `music` |
-- | `!.mp3$`  | inverse-suffix-exact-match | Items that do not end with `.mp3`    |
require("telescope").load_extension("ui-select")
require("telescope").load_extension("zf-native")

local luasnip = require("luasnip")
local cmp = require("cmp")
local lspconfig = require("lspconfig")

function _G.grep_string()
  require("telescope.builtin").grep_string {
    path_display = "smart",
    word_match = "-w",
    sort_only_text = false,
    search = "",
  }
end

function _G.live_grep()
  require("telescope.builtin").live_grep {
    disable_coordiantes = true,
  }
end

-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

local severity = vim.diagnostic.severity
vim.diagnostic.config {
  underline = false,
  signs = true,
  update_in_insert = false,
  severity_sort = true,
  float = {
    focusable = false,
    source = "always",
  },
  virtual_text = {
    spacing = 4,
    source = "always",
    severity = {
      min = vim.diagnostic.severity.ERROR,
    },
  },
}

-- after the language server attaches to the current buffer
-- fzf_lspp
require("fzf_lsp").setup()

local diag_err = { severity = severity.ERROR }
local diag_wrn = { severity = severity.WARN }
-- function goto
nmap("<space>e", vim.diagnostic.open_float)
nmap("[e", function() vim.diagnostic.goto_prev(diag_err) end)
nmap("]e", function() vim.diagnostic.goto_next(diag_err) end)
nmap("[d", vim.diagnostic.goto_prev)
nmap("]d", vim.diagnostic.goto_next)
nmap("<space>q", vim.diagnostic.setloclist)

-- function peek_definition(buf)
--   local params = vim.lsp.util.make_position_params()
--   return vim.lsp.buf_request(buf, "textDocument/typeDefinition", params, preview_location_callback)
-- End

local rainbow = require("rainbow-delimiters")
nmap("<leader>=", rainbow.toggle)
local rainbow_delimiters = require("rainbow-delimiters")
vim.g.rainbow_delimiters = {
  strategy = {
    [""] = rainbow_delimiters.strategy["local"],
  },
  query = {
    [""] = "rainbow-delimiters",
    lua = "rainbow-blocks",
  },
  priority = {
    [""] = 110,
    lua = 210,
  },
  highlight = {
    "RainbowDelimiterRed",
    "RainbowDelimiterYellow",
    "RainbowDelimiterBlue",
    "RainbowDelimiterOrange",
    "RainbowDelimiterGreen",
    "RainbowDelimiterViolet",
    "RainbowDelimiterCyan",
  },
}

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd("LspAttach", {
  group = vim.api.nvim_create_augroup("UserLspConfig", {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"
    local client = vim.lsp.get_client_by_id(ev.data.client_id)
    local buf = vim.lsp.buf
    local function toggle_inlay_hints()
      local filter = { bufnr = ev.buf }
      vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled(filter), filter)
    end

    ---@diagnostic disable-next-line: need-check-nil
    client.server_capabilities.semanticTokensProvider = nil

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    local function nmap_buf(key, cmd) nmap(key, cmd, opts) end
    -- nmap("<<space>-D>", peek_definition)
    nmap_buf("<space>d", buf.declaration)
    nmap_buf("<leader>h", toggle_inlay_hints)
    nmap_buf("gd", buf.definition)
    nmap_buf("gd", buf.definition)
    nmap_buf("K", buf.hover)
    nmap_buf("gi", buf.implementation)
    nmap_buf("<C-k>", buf.signature_help)
    nmap_buf("<space>wa", buf.add_workspace_folder)
    nmap_buf("<space>wr", buf.remove_workspace_folder)
    nmap_buf("<space>wl", function() print(vim.inspect(buf.list_workspace_folders())) end)
    nmap_buf("gD", buf.type_definition)
    nmap_buf("gr", buf.rename)
    vim.keymap.set({ "n", "v" }, "<space>ca", buf.code_action, opts)
    nmap_buf("<space>f", function() buf.code_action { apply = true } end)
    nmap_buf("<space>e", vim.diagnostic.open_float)
    nmap_buf("<space>r", buf.references)
    nmap_buf("<space>F", function() buf.format { async = true } end)
    nmap_buf("<space>h", buf.document_highlight)
  end,
})
-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

local servers = {
  nil_ls = {},
  -- uncomment when done with treesitter development
  -- tsserver = {},
  svelte = {},
  clangd = {},
  openscad_lsp = {
    settings = {
      openscad = {
        fmt_exe = "scadformat",
        fmt_args = { "--log-level error" },
      },
    },
  },
  nickel_ls = {},
  -- rust_analyzer = {
  --   settings = {
  --     ["rust-analyzer"] = {
  --       cargo = { loadOutDirsFromCheck = true },
  --       procMacro = { enable = true },
  --       hover = { linksInHover = true },
  --     },
  --   },
  -- },
  gopls = {
    settings = {
      gopls = {
        linksInHover = false,
      },
    },
  },
  pyright = {},
  ts_ls = {},

  lua_ls = {
    settings = {
      Lua = {
        diagnostics = {
          globals = { "vim" },
        },
        workspace = {
          -- Make the server aware of Neovim runtime files
          library = vim.api.nvim_get_runtime_file("", true),
          checkThirdParty = false,
        },
        runtime = {
          version = "LuaJIT",
          path = runtime_path,
        },
        -- Do not send telemetry data containing a randomized but unique identifier
        telemetry = { enable = false },
      },
    },
  },
  bashls = {},
}

for server, cfg in pairs(servers) do
  local setup = {
    capabilities = capabilities,
  }
  for k, v in pairs(cfg) do
    setup[k] = v
  end
  lspconfig[server].setup(setup)
end

require("_lsp")
require("_ts_grammar")
lspconfig.tree_sitter_grammar_lsp.setup {}
vim.filetype.add {
  extension = {
    corpus = "corpus",
  },
  filename = {
    ["~/.kube/config"] = "yaml",
  },
  pattern = {
    [".*%.scm"] = "query",
    [".*%.scad"] = "openscad",
    [".*%.ttl"] = "turtle",
    [".*/templates/.*%.yaml"] = "gotmpl",
    [".*/test/corpus/.*%.txt"] = { "corpus", priority = 10 },
    ["flake.lock"] = "json",
    ["uv.lock"] = "toml",
  },
}

local lspkind_comparator = function(conf)
  local lsp_types = require("cmp.types").lsp
  return function(entry1, entry2)
    if entry1.source.name ~= "nvim_lsp" then
      if entry2.source.name == "nvim_lsp" then
        return false
      else
        return nil
      end
    end
    local kind1 = lsp_types.CompletionItemKind[entry1:get_kind()]
    local kind2 = lsp_types.CompletionItemKind[entry2:get_kind()]

    local priority1 = conf.kind_priority[kind1] or 0
    local priority2 = conf.kind_priority[kind2] or 0
    if priority1 == priority2 then return nil end
    return priority2 < priority1
  end
end

local label_comparator = function(entry1, entry2) return entry1.completion_item.label < entry2.completion_item.label end

-- luasnip setup
cmp.setup {
  snippet = {
    expand = function(args) require("luasnip").lsp_expand(args.body) end,
  },
  mapping = cmp.mapping.preset.insert {
    ["<Up>"] = cmp.mapping.select_prev_item(),
    ["<Down>"] = cmp.mapping.select_next_item(),
    ["<C-d>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-Space>"] = cmp.mapping.complete(),
    ["<C-e>"] = cmp.mapping.close(),
    ["<CR>"] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ["<Tab>"] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end,
    ["<S-Tab>"] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end,
  },
  sources = {
    { name = "buffer" },
    { name = "nvim_lsp" },
    { name = "luasnip" },
    { name = "path" },
  },
  sorting = {
    comparators = {
      lspkind_comparator {
        kind_priority = {
          Field = 11,
          Property = 11,
          Constant = 10,
          Enum = 10,
          EnumMember = 10,
          Event = 10,
          Function = 10,
          Method = 10,
          Operator = 10,
          Reference = 10,
          Struct = 10,
          Variable = 12,
          File = 8,
          Folder = 8,
          Class = 5,
          Color = 5,
          Module = 5,
          Keyword = 2,
          Constructor = 1,
          Interface = 1,
          Snippet = 0,
          Text = 1,
          TypeParameter = 1,
          Unit = 1,
          Value = 1,
        },
      },
      label_comparator,
    },
  },
}

-- cmp.setup.cmdline('/', {
-- sources = {
-- { name = 'buffer' }
-- }
-- })

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(":", {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = "path" },
  }, {
    { name = "cmdline" },
  }),
})

-- require("nvim-treesitter.install").compilers = { "gcc-12" }

require("_treesitter")
require("_theme")

require("lualine").setup {
  options = {
    icons_enabled = true,
    theme = "onedark",
    component_separators = { left = "", right = "" },
    section_separators = { left = "", right = "" },
    disabled_filetypes = {
      statusline = {},
      winbar = {},
    },
    ignore_focus = {},
    always_divide_middle = false,
    globalstatus = false,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 1000,
    },
  },
  sections = {
    lualine_a = {},
    lualine_b = {
      {
        "branch",
        max_length = vim.o.columns / 8,
        windows_color = {
          -- Same values as the general color option can be used here.
          active = "lualine_{section}_inactive", -- Color for active window.
          inactive = "lualine_{section}_inactive", -- Color for inactive window.
        },
      },
      { "filename", path = 1 },
    },
    lualine_c = {},
    lualine_x = {},
    lualine_y = {},
    lualine_z = { "location" },
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = { { "filename", path = 1 } },
    lualine_x = { "location" },
    lualine_y = {},
    lualine_z = {},
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
  extensions = {},
}

local function preview_location_callback(_, method, result)
  print(table.dump(method))
  if result == nil or vim.tbl_isempty(result) then
    print(method, "No location found")
    return nil
  end
  vim.lsp.util.preview_location(method)
  if vim.tbl_islist(result) then
    print("islist")
    vim.lsp.util.preview_location(result[1])
  else
    print(table.dump(result))
    vim.lsp.util.preview_location(result)
  end
end

function lsp_clients()
  local clients = vim.inspect(vim.lsp.get_active_clients())

  vim.api.nvim_command([[ new ]])
  vim.api.nvim_paste(clients, "", -1)
end

command("WorkspaceReload", function() vim.cmd.RustLsp("reloadWorkspace") end, {})
command("MacroExpand", function() vim.cmd.RustLsp("expandMacro") end, {})
command("Dir", function() print(vim.loop.cwd()) end, {})
command("File", function() print(vim.fn.expand("%:p")) end, {})
nmap("<leader>d", function() print(vim.loop.cwd()) end)
-- nmap("<C-f>r", function() require("fzf-lua").grep_project() end, {})
nmap("<leader>f", function() print(vim.fn.expand("%:p")) end)
nmap("*", function()
  -- if a count was supplied, execute * normally and exit
  if vim.v.count > 0 then
    vim.cmd("normal! " .. vim.v.count .. "*<CR>")
    return
  end

  -- save current window view
  local windowView = vim.fn.winsaveview()

  -- execute * normally
  vim.cmd("silent keepjumps normal! *<CR>")

  -- restore the window view
  if windowView ~= nil then vim.fn.winrestview(windowView) end
end)
