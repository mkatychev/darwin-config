parser_config = require("nvim-treesitter.parsers").get_parser_configs()
parser_config.just = {
  install_info = {
    url = "/Users/mkatychev/Documents/tree-sitter/just", -- local path or git repo
    files = { "src/parser.c", "src/scanner.c" },
    -- use_makefile = true -- this may be necessary on MacOS (try if you see compiler errors)
  },
  maintainers = { "@IndianBoy42" },
}
parser_config.gotmpl = {
  install_info = {
    url = "https://github.com/ngalaiko/tree-sitter-go-template",
    files = { "src/parser.c" },
  },
  filetype = "gotmpl",
  used_by = { "gohtmltmpl", "gotexttmpl", "gotmpl", "yaml" },
}
parser_config.wit = {
  install_info = {
    url = "/Users/mkatychev/Documents/tree-sitter/wit",
    files = { "src/parser.c" },
  },
}



-- parser_config.rust = {
--   install_info = {
--     url = "https://github.com/tree-sitter/tree-sitter-rust",
--     files = { "src/parser.c", "src/scanner.c" },
--   },
-- }

parser_config.turtle = {
  install_info = {
    url = "/Users/mkatychev/Documents/tree-sitter/turtle", -- local path or git repo
    files = { "src/parser.c" },
  },
}

parser_config.openscad = {
  install_info = {
    url = "/Users/mkatychev/Documents/tree-sitter/openscad", -- local path or git repo
    files = { "src/parser.c" },
  },
  filetype = "openscad",
}

-- parser_config.test = {
--   install_info = {
--     url = "https://github.com/tree-sitter-grammars/tree-sitter-test", -- local path or git repo
--   },
--   filetype = "test",
-- }
