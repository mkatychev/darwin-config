local textobject_all = function(prefix, inner, outer)
  local both = {
    ["B"] = "block",
    ["C"] = "call",
    ["c"] = "class",
    ["c"] = "class",
    ["b"] = "conditional",
    ["f"] = "function",
    ["l"] = "loop",
    ["p"] = "parameter",
    ["a"] = "assignment",
  }
  local custom = {
    ["/"] = "comment.outer",
    ["t"] = "statement.outer",
    ["v"] = "variable",
    ["g"] = "function_block",
  }
  local output = {}
  for k, v in pairs(both) do
    output[prefix .. inner .. k] = string.format("@%s.inner", v)
    output[prefix .. outer .. k] = string.format("@%s.outer", v)
  end
  for k, v in pairs(custom) do
    output[prefix .. outer .. k] = string.format("@%s", v)
  end
  return output
end

-- TreeSitter
local textobject_outer = function(prefix)
  return {
    [prefix .. "B"] = "@block.outer",
    [prefix .. "C"] = "@call.outer",
    [prefix .. "c"] = "@class.outer",
    [prefix .. "/"] = "@comment.outer",
    [prefix .. "b"] = "@conditional.outer",
    [prefix .. "f"] = "@function.outer",
    [prefix .. "g"] = "@function_block",
    [prefix .. "l"] = "@loop.outer",
    [prefix .. "p"] = "@parameter.outer",
    [prefix .. "t"] = "@statement.outer",
    [prefix .. "a"] = "@match_arm",
  }
end

require("nvim-treesitter.configs").setup {
  sync_install = false,
  matchup = { enable = true },
  indent = { enable = true },
  -- ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  ignore_install = { "swift", "phpdoc" },
  highlight = {
    enable = true, -- false will disable the whole extension
    -- disable = { "markdown_inline" },
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gni",
      node_incremental = "gnn",
      scope_incremental = "gno",
      node_decremental = "gnd",
    },
  },
  textobjects = {
    lsp_interop = {
      enable = true,
      border = "none",
      peek_definition_code = {
        ["<space>k"] = "@function.outer",
        ["<space>c"] = "@class.outer",
        ["<space>C"] = "@call.outer",
        ["<space>t"] = "@statement.outer",
        ["<space>p"] = "@parameter.outer",
      },
    },

    select = {
      enable = true,
      -- You can use the capture groups defined in textobjects.scm
      keymaps = textobject_all("", "i", "a"),
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = textobject_all("]", "i", ""),
      goto_next_end = textobject_all("]e", "i", ""),
      goto_previous_start = textobject_all("[", "i", ""),
      goto_previous_end = textobject_all("[e", "i", ""),
    },
    swap = {
      enable = true,
      swap_next = textobject_all("}", "i", "a"),
      swap_previous = textobject_all("{", "i", "a"),
    },
  },
}

vim.treesitter.language.register("bash", "zsh")
vim.treesitter.language.register("python", "python3")
