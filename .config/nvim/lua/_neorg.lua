local M = {}
function M.setup()
  require("neorg").setup {
    load = {
      ["core.defaults"] = {}, -- Loads default behaviour
      ["core.journal"] = {
        config = {
          strategy = "flat",
          workspace = "notes",
        },
      }, -- Loads default behaviour
      ["core.concealer"] = {}, -- Adds pretty icons to your documents
      ["core.dirman"] = { -- Manages Neorg workspaces
        config = {
          default_workspace = "notes",
          workspaces = {
            notes = "~/Documents/notes",
          },
        },
      },
    },
  }
end

return M
