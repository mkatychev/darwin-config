vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = { "grammar.js" },
  callback = function() vim.cmd("setlocal filetype=ts-grammar") end,
})

vim.treesitter.language.register("javascript", "ts-grammar")

local configs = require("lspconfig.configs")
local util = require("lspconfig.util")
configs.tree_sitter_grammar_lsp = {
  default_config = {
    cmd = { "ts-grammar-lsp" }, -- update to match your local installation location
    filetypes = { "ts-grammar" },
    root_dir = util.path.dirname,
  },
}

